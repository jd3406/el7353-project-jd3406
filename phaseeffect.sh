#!/bin/bash

# a is the min ratio multiple 1000;
# b is the max ratio multiple 1000;
# c is the step of ratio multiple 1000. 
a=980
b=2000
c=1

while [ $a -lt $b ]
do
    tim=$(echo "($a * 221 - 211000)" | bc)
    echo $(sudo tc qdisc change dev eth1 root netem delay $tim)
    echo $(tc qdisc show dev eth1)
    sleep 100
    let "a = $a + $c"
done




