## NYU Poly  EL7353 Network Modeling and Analysis  Project
# Network traffic models: Internet Research Needs Better Models


## 1. Introduction 
In S. Floyd’s paper, they discuss some modeling issues from other network research which has flaws due to inappropriate network models. My project includes two examples of these researches from the “Rogues Gallery”. The first part “Phase Effects” is about some results showing one discriminatory behavior of a network with Drop Tail gateway and TCP congestion control. The second part “AQM Oscillations” is about some queue oscillations with Adaptive RED due to some moderate changes in the traffic mix. I use the GENI testbed to reproduce the simulation results from S. Floyd. It will take about two hours to run my experiment and reproduce my figures.  


## 2. Background 
### Phase Effects

![fo1.jpg](https://bitbucket.org/repo/ykR8qM/images/1800592873-fo1.jpg)

Some simulations which demonstrate sensitive dependence on precise parameter settings, are artifacts of unrealistic simulation scenarios, such as those with long-lived traffic, packets the same size, and no reverse-path traffic. From paper “On Traffic Phase Effects in Packet-Switched Gateways”, Figure 1 shows that a small change in the propagation delay of a single link completely changes the fraction of link bandwidth received by one of two TCP flows sharing a Drop-Tail queue. The x-axis is the round-trip-time ratio of node 2 to node 1 in each simulation, while the y-axis is node 1’s average throughput in each simulation. 


### Active Queue Management: Oscillations

![fo2.jpg](https://bitbucket.org/repo/ykR8qM/images/1114056482-fo2.jpg)
![fo3.jpg](https://bitbucket.org/repo/ykR8qM/images/1141449015-fo3.jpg)
![fo4.jpg](https://bitbucket.org/repo/ykR8qM/images/3720675070-fo4.jpg)

In active queue management, it is difficult to decide which simulation scenarios are the most important to address. Researches of Queue oscillations with RED often have scenarios with one-way, long-lived traffic; however, moderate changes in the traffic mix can strongly affect oscillation dynamics, such as adding short-lived flows, reverse-path traffic, and a range of round-trip times, which means different models can affect experiment dynamics. The following three figures share the same dumbbell topology with Adaptive RED queue management. The x-axis is the simulation time, while the y-axis is the instantaneous queue size, and the average queue size estimated by RED. Figure 2 only has long-lived traffic and constant round-trip time, but Figure 3 adds a range of round-trip time, and Figure 4 even adds some web traffic. 

## 3. Results 
### Phase Effests

![fs1.jpg](https://bitbucket.org/repo/ykR8qM/images/3831068166-fs1.jpg)

From the figure above, each dot represents the result of a single trial, and we can see as the ratio increases from 1.0 to 2.0, the throughput of flow 1 is changing and shifting from low point to high point with a trend of increasing. 
At point about 1.1 to 1.4, when the propagation delays of the two competing flows’ access links are equal, both flows have the same round-trip time and receive the same fraction of the link bandwidth. At 1.6 to 2.0, as the propagation delay of Node 2 increases more, flow 1 can receive almost all of the link bandwidth at most of the time, but at some special points, it also suddenly receive less of the link bandwidth, depending on the exact propagation delays of the two access links. 

The author says that in real network, the phase effects will be less likely, which means that the tricky simulation can make the phase effects characteristic. From the picture below we only observe the increasing and decreasing of throughput due to the round-trip time. But from the picture above, we can observe some shifts as the ratio increases perhaps due to the setup and termination of TCP connection, and also we can observe the throughput is increasing as the ratio increases, which means the trend in the picture above and the trend in the picture below are similar. Therefore, according to my experiment results, I think there might be no such phase effects for the testbed experiment as the original paper said for the simulation. 


### Active Queue Management: Oscillations

![fs22.jpg](https://bitbucket.org/repo/ykR8qM/images/3747931513-fs22.jpg)
![fs32.jpg](https://bitbucket.org/repo/ykR8qM/images/1426730170-fs32.jpg)
![fs42.jpg](https://bitbucket.org/repo/ykR8qM/images/2934646295-fs42.jpg)

There are three figures below. In every figure, there are 1600 points from the last 50 seconds of every trial. In the first figure, it almost changes between 55 and 65 with a highly stability and regularity, like a small oscillation. In the second figure, a range of round-trip time is added, so the small oscillations in queue size have been replaced by more irregular behavior. It changes almost between 30 and 50. In the third figure, after adding the drop rate, the queue dynamics is rather different. The queue size varies more extremely between 25 and 60. 

I think the main idea that the original paper wants to convey is that the queue oscillations are not regular after adding a range of round-trip time and short-lived flows. My figures also show the absence of regular oscillations of the queue size when the traffic mix changes. I think the original paper says that the traffic of real network will make the oscillation characteristic in simulation experiment change into burst behavior. And the results of my testbed experiment are more like to the burst behavior. 

## 4. Run my experiment 
### Phase Effects

(1) Set up topology and install required software

![f1.jpg](https://bitbucket.org/repo/ykR8qM/images/1990318631-f1.jpg)

Create a new slice on the "GENI" portal and a "two sources-gateway-sink" topology in the slice, then choose an proper aggregate. The links between source and gateway is 8000 kbps bandwidth, while the link between gateway and sink is 800 kbps bandwidth. You also can use the "project1_request_rspec.xml" document directly. Remember to install **iperf** on the source and sink nodes. The code is as follow. 

    ssh UserName@HostName -p PortNumber -i /Path/Key
    sudo apt-get update
    sudo apt-get install iperf


(2) Set up Drop Tail gateway and TCP congestion control 

Log into both the sources and sink, first use **sysctl** to see the TCPs that are available in the nodes. Then choose **reno** to set the behavior of TCP.  

    sysctl net.ipv4.tcp_available_congestion_control
    sudo sysctl -w net.ipv4.tcp_congestion_control=reno

At the right interface of the gateway node, set up the queue by **netem** with a delay of 200 ms plus a gateway delay of 10ms, and buffer size of 15 packets.  

    sudo tc qdisc add dev eth3 root netem delay 210ms limit 15

At the right interfaces of both two source nodes, set up a initial delay of 10 ms. 

    sudo tc qdisc add dev eth1 root netem delay 10ms

(3) Send TCP packets

At first, run **iperf -s** to let the sink to listen with a certain window size. Then for the two sources, run **iperf -c** one by one to let them send TCP flows to the sink together. Because the required experiment time is 100 seconds, while the two sources can't begin at the same time, the experiment time is increased to 120 seconds, and the output of average throughput is shown every 50 seconds. The required window size at the sources is 32 packets, but some Linux system will double it automatically, so be careful of this change. The packet size is 1000 bytes. After the sending finishes, remember to use **Ctrl+C** to terminate the sink's listen. 

    iperf -s -w 32k -i 50
    iperf -c sink -w 16k -l 960 -t 120 -i 50

(4) Change the round-trip time of source 2

The round-trip-time ratio of node 2 to node 1 is changed from 0.955 to 1.995 step by 0.02, and the propagation delay between source 2 and gateway equals to (ratio*221-211), so there are total 53 trials. First, change the delay at the right interface of source 2. Then repeat the former step to send packets, and record the throughput of the second 50 seconds interval. The specific value of the delay can be seen in the "data.xlsx" document. 

    sudo tc qdisc change dev eth1 root netem delay 229.9ms

In order to make it easy to reproduce, I write a bash script **phaseeffect.sh** for the source2. You can set and change the range and the step of the ratio, and run the experiment automatically. An example is as follow, if you want to measure the throughput when the ratio change from 1 to 2 step by 0.001 as the original paper. 

    nano phaseeffect.sh # at the source2, set a=1000, b=2000, c=1, d=b+1
    iperf -s -w 16k -i 50 # at the sink
    iperf -c sink -w 16k -l 960 -t 200200 -i 50 # at the source1
    iperf -c sink -w 16k -l 960 -t 200200 -i 50 # at the source2
    ./phaseeffect.sh # at the source2 of a new terminal 

### Active Queue Management: Oscillations

(1) Set up topology and install required software

![f2.jpg](https://bitbucket.org/repo/ykR8qM/images/666706634-f2.jpg)

Create a new slice on the "GENI" portal and a "sender-router1-router2-receiver" topology in the slice, then choose an proper aggregate. The link between two router is 15 Mbps bandwidth. ou also can use the "project2_request_rspec.xml" document directly. Remember to install **iperf** on the source and sink nodes. The code is as follow. 

    ssh UserName@HostName -p PortNumber -i /Path/Key
    sudo apt-get update
    sudo apt-get install iperf

(2) Set up the Adaptive RED queue management

At router 1, set up the **red** queue in the **tbf** queue, which has the rate of 1 Mbit, the burst of 15 kB, the latency of 10ms, the peak rate of 1.01 Mbit and the MTU of 1600. The Adaptive RED has the limit of 150 kB, the min of 30 kB, the AVPKT of 1000, and the bandwidth of 15 Mbit.

    sudo tc qdisc add dev eth1 root handle 1: tbf rate 1mbit burst 15kB latency 10ms peakrate 1.01mbit mtu 1600 
    sudo tc qdisc add dev eth1 parent 1: handle 2: red limit 150000 min 30000 avpkt 1000 ecn bandwidth 15Mbit adaptive

At router 2, set up the propagation delay of 240 ms, including the nature delay (1.5 ms) of the whole topology. 

    sudo tc qdisc add dev eth1 root netem delay 238.5ms

(3) Send TCP flows

At the receiver, run **iperf -s**. At the sender, run the follow code to send 80 TCP flows with packet size of 1000 bytes, and window size of 20 packets, and experiment time of 120 seconds. Later, no more than ten seconds, run **queuemonitor** at router 1 to record the instantaneous queue size. 

    iperf -c receiver -P 80 -t 120 -l 960 -w 20k      
    ./queuemonitor.sh eth1 100 0.02 | tee router.txt     

For the second trial, change the round-trip time at the right interface of router 2. Then repeat the former step to send 80 TCP flows.

    sudo tc qdisc change dev eth1 root netem delay 238.5ms 220ms 

For the third trial, keep the change in the second trial, and generate web traffic with 15 percent long-lived flows (I am confused). The following code is tried to make some flows not live long. 

    sudo tc qdisc change dev eth1 root netem delay 238.5ms 220ms corrupt 15%

### Notes 

I used "Git Bash" on "Windows 7" to run the experiment. The kernel version was "Ubuntu 14.04.1 LTS (GNU/Linux 3.13.0-33-generic x86_64)". In "Phase Effects", I used "CENIC InstaGENI" aggregate. In "AQM Oscillations", I used "Clemson InstaGENI" aggregate.(For the new results in the final draft, I used "NYU InstaGENI" for both.) In my experiment, I tried to use "nuttcp" and "iperf". In "Phase Effects", the original paper used about 1100 simulations, and I tried to write a bash script to run it automatically but didn't succeed, so I sampled and reduced the trials by 20 times. During my experiment, the ability of the testbed is good to support this kind of experiment. I didn't need to scale down the original experiment design. GENI aggregates could provide the number of nodes and the high-capacity links of these experiment.